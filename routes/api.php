<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

Route::post('login', 'AuthController@login')->name('login');
Route::post('register', 'AuthController@register')->name('register');

Route::get('tags/{tag}/posts', 'PostTagController@getPostsByTag')->name('tags.posts');
Route::get('categories/{category}/posts/', 'PostCategoryController@getPostsByCategory')->name('categories.posts');
Route::apiResource('posts/tags', 'PostTagController')->only(['index']);
Route::apiResource('posts/categories', 'PostCategoryController')->only(['index', 'show']);
Route::apiResource('posts', 'PostController')->only(['index', 'show']);

Route::apiResource('comments', 'CommentController')->only(['index', 'show']);

Route::middleware('ms-auth')->group(function () {
    Route::post('logout', 'AuthController@logout')->name('logout');
    Route::get('users/current', 'AuthController@currentUser')->name('current-user');

    Route::apiResource('posts/categories', 'PostCategoryController')->except(['index', 'show']);
    Route::apiResource('posts', 'PostController')->except(['index', 'show']);
    Route::post('posts/{post}/toggle-like', 'PostController@toggleLike')->name('posts.toggle-like');

    Route::get('comments/{comment}/childes', 'CommentController@showChildes')->name('comments.childes');
    Route::apiResource('comments', 'CommentController')->except(['index', 'show']);
    Route::post('comments/{comment}/toggle-like', 'CommentController@toggleLike')->name('comments.toggle-like');

    Route::apiResource('files', 'FileController')->except(['update']);
});
