<?php
return [
    'auth_domain' => env('AUTH_DOMAIN'),
    'posts_domain' => env('POSTS_DOMAIN'),
    'comments_domain' => env('COMMENTS_DOMAIN'),
    'files_domain' => env('FILES_DOMAIN'),
];
