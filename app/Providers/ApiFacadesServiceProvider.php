<?php

declare(strict_types=1);

namespace App\Providers;

use App\Apis\AuthApi;
use App\Apis\CommentApi;
use App\Apis\FileApi;
use App\Apis\PostApi;
use Illuminate\Support\Facades\App;
use Illuminate\Support\ServiceProvider;

class ApiFacadesServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        App::bind('auth-api', function () {
            return new AuthApi();
        });
        App::bind('comments-api', function () {
            return new CommentApi();
        });
        App::bind('posts-api', function () {
            return new PostApi();
        });
        App::bind('files-api', function () {
            return new FileApi();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
