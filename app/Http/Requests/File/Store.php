<?php

declare(strict_types=1);

namespace App\Http\Requests\File;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    public function rules(): array
    {
        return [
            'file' => [
                'required',
                'file',
                'mimes:jpg,jpeg,png',
                'max:2058'
            ],
            'type' => [
                'required',
                'string',
            ],
            'fileable_type' => [
                'required',
                'string'
            ],
            'fileable_id' => [
                'required',
                'integer'
            ]
        ];
    }
}
