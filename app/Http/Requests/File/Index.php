<?php

declare(strict_types=1);

namespace App\Http\Requests\File;

use Illuminate\Foundation\Http\FormRequest;

class Index extends FormRequest
{
    public function rules(): array
    {
        return [
            'fileable_type' => [
                'required',
            ],
            'fileable_id' => [
                'required',
                'integer'
            ]
        ];
    }
}
