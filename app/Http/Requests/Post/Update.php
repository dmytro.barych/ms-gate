<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function rules(): array
    {
        return [
            'title' => [
                'sometimes',
                'min:3'
            ],
            'type' => [
                'sometimes',
            ],
            'description' => [
                'sometimes'
            ]
        ];
    }
}
