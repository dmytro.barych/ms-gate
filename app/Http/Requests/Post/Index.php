<?php

declare(strict_types=1);

namespace App\Http\Requests\Post;

use Illuminate\Foundation\Http\FormRequest;

class Index extends FormRequest
{
    public function rules(): array
    {
        return [
            'page' => [
                'sometimes',
                'integer'
            ]
        ];
    }
}
