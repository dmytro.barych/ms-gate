<?php

declare(strict_types=1);

namespace App\Http\Requests\PostCategory;

use Illuminate\Foundation\Http\FormRequest;

class Update extends FormRequest
{
    public function rules(): array
    {
        return [
            'name' => ['required', 'min:3']
        ];
    }
}
