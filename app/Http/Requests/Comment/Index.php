<?php

declare(strict_types=1);

namespace App\Http\Requests\Comment;

use Illuminate\Foundation\Http\FormRequest;

class Index extends FormRequest
{
    public function rules(): array
    {
        return [
            'post_ids' => [
                'required',
                'array'
            ],
            'post_ids.*' => [
                'required',
                'integer',
            ],
            'per_page' => [
                'sometimes',
                'integer'
            ],
            'page' => [
                'sometimes',
                'integer'
            ]
        ];
    }
}
