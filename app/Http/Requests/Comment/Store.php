<?php

declare(strict_types=1);

namespace App\Http\Requests\Comment;

use Illuminate\Foundation\Http\FormRequest;

class Store extends FormRequest
{
    public function rules(): array
    {
        return [
            'post_id' => [
                'required',
                'integer'
            ],
            'text' => [
                'required',
                'min:3'
            ],
            'parent_id' => [
                'sometimes',
                'integer',
            ]
        ];
    }
}
