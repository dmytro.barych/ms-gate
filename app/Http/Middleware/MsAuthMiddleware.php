<?php

declare(strict_types=1);

namespace App\Http\Middleware;

use App\Facades\AuthApi;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class MsAuthMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if ($request->header('Authorization')) {
            $response = AuthApi::user($request->header('Authorization'));

            if (isset($response['data'])) {
                request()->authUser = $response['data'];
                return $next($request);
            }
        }
        return response(['Token mismatch'], Response::HTTP_UNAUTHORIZED);
    }
}
