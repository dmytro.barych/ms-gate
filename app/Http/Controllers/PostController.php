<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Facades\PostApi;
use App\Http\Requests\Post\Index;
use App\Http\Requests\Post\Store;
use App\Http\Requests\Post\Update;
use Illuminate\Http\JsonResponse;

class PostController extends Controller
{
    public function index(Index $request): JsonResponse
    {
        $response = PostApi::index($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function store(Store $request): JsonResponse
    {
        $response = PostApi::store($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function show(int $id): JsonResponse
    {
        $response = PostApi::show($id);
        return response()->json($response->json(), $response->status());
    }

    public function update(Update $request, int $id): JsonResponse
    {
        $response = PostApi::update($id, $request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function destroy(int $id): JsonResponse
    {
        $response = PostApi::destroy($id);
        return response()->json($response->json(), $response->status());
    }

    public function toggleLike(int $id): JsonResponse
    {
        $response = PostApi::toggleLike($id);
        return response()->json($response->json(), $response->status());
    }
}
