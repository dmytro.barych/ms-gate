<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Facades\AuthApi;
use App\Http\Requests\Auth\Login;
use App\Http\Requests\Auth\Register;
use Illuminate\Http\JsonResponse;

class AuthController extends Controller
{
    public function login(Login $request): JsonResponse
    {
        $response = AuthApi::login($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function register(Register $request): JsonResponse
    {
        $response = AuthApi::register($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function logout(): JsonResponse
    {
        $response = AuthApi::logout(request()->header('Authorization'));
        return response()->json($response->json(), $response->status());
    }

    public function currentUser(): JsonResponse
    {
        $response = AuthApi::user(request()->header('Authorization'));
        return response()->json($response->json(), $response->status());
    }
}
