<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Facades\CommentApi;
use App\Http\Requests\Comment\Index;
use App\Http\Requests\Comment\Store;
use App\Http\Requests\Comment\Update;
use Illuminate\Http\JsonResponse;

class CommentController extends Controller
{
    public function index(Index $request): JsonResponse
    {
        $response = CommentApi::index($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function store(Store $request): JsonResponse
    {
        $response = CommentApi::store($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function show(int $id): JsonResponse
    {
        $response = CommentApi::show($id);
        return response()->json($response->json(), $response->status());
    }

    public function showChildes(int $id): JsonResponse
    {
        $response = CommentApi::showChildes($id);
        return response()->json($response->json(), $response->status());
    }

    public function update(Update $request, int $id): JsonResponse
    {
        $response = CommentApi::update($id, $request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function destroy(int $id): JsonResponse
    {
        $response = CommentApi::destroy($id);
        return response()->json($response->json(), $response->status());
    }

    public function toggleLike(int $id): JsonResponse
    {
        $response = CommentApi::toggleLike($id);
        return response()->json($response->json(), $response->status());
    }
}
