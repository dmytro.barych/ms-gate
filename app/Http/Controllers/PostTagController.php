<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Facades\PostApi;
use Illuminate\Http\JsonResponse;

class PostTagController extends Controller
{
    public function index(): JsonResponse
    {
        $response = PostApi::categoryIndex();
        return response()->json($response->json(), $response->status());
    }

    public function getPostsByTag(int $id): JsonResponse
    {
        $response = PostApi::getPostsByTag($id);
        return response()->json($response->json(), $response->status());
    }
}
