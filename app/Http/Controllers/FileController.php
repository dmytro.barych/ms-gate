<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Facades\FileApi;
use App\Http\Requests\File\Index;
use App\Http\Requests\File\Store;
use Illuminate\Http\JsonResponse;

class FileController extends Controller
{
    public function index(Index $request): JsonResponse
    {
        $response = FileApi::index($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function store(Store $request): JsonResponse
    {
        $response = FileApi::store($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function show(int $id): JsonResponse
    {
        $response = FileApi::show($id);
        return response()->json($response->json(), $response->status());
    }

    public function destroy(int $id): JsonResponse
    {
        $response = FileApi::destroy($id);
        return response()->json($response->json(), $response->status());
    }
}
