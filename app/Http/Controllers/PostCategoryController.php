<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Facades\PostApi;
use App\Http\Requests\PostCategory\Store;
use App\Http\Requests\PostCategory\Update;
use Illuminate\Http\JsonResponse;

class PostCategoryController extends Controller
{
    public function index(): JsonResponse
    {
        $response = PostApi::categoryIndex();
        return response()->json($response->json(), $response->status());
    }

    public function store(Store $request): JsonResponse
    {
        $response = PostApi::categoryStore($request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function show(int $id): JsonResponse
    {
        $response = PostApi::categoryShow($id);
        return response()->json($response->json(), $response->status());
    }

    public function update(Update $request, int $id): JsonResponse
    {
        $response = PostApi::categoryUpdate($id, $request->validated());
        return response()->json($response->json(), $response->status());
    }

    public function destroy(int $id): JsonResponse
    {
        $response = PostApi::categoryDestroy($id);
        return response()->json($response->json(), $response->status());
    }

    public function getPostsByCategory(int $id): JsonResponse
    {
        $response = PostApi::getPostsByCategory($id);
        return response()->json($response->json(), $response->status());
    }
}
