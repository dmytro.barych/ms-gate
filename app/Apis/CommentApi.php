<?php

declare(strict_types=1);

namespace App\Apis;

use Illuminate\Support\Facades\Http;

class CommentApi
{
    private $client;

    public function __construct()
    {
        $this->client = Http::withHeaders(
            [
                'Content-Type' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
                'Authorization' => request()->header('authorization') ?? null
            ]
        )->baseUrl(config('domains.comments_domain'));
    }

    public function index(array $data)
    {
        return $this->client->withBody(
            json_encode($data), 'application/json'
        )->get('/comments');
    }

    public function store(array $data)
    {
        return $this->client->post('/comments', array_merge(['user_id' => request()->authUser['id']], $data));
    }

    public function show(int $id)
    {
        return $this->client->get("/comments/$id");
    }

    public function showChildes(int $id)
    {
        return $this->client->get("/comments/$id/childes");
    }

    public function update(int $id, array $data)
    {
        return $this->client->put("/comments/$id", $data);
    }

    public function destroy(int $id)
    {
        return $this->client->delete("/comments/$id");
    }

    public function toggleLike(int $id)
    {
        return $this->client->post("comments/$id/toggle-like", ['user_id' => request()->authUser['id']]);
    }
}
