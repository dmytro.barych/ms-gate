<?php

declare(strict_types=1);

namespace App\Apis;

use Illuminate\Support\Facades\Http;

class AuthApi
{
    private $client;

    public function __construct()
    {
        $this->client = Http::withHeaders(
            ['Content-Type' => 'application/json', 'X-Requested-With' => 'XMLHttpRequest']
        )->baseUrl(config('domains.auth_domain'));
    }

    public function user(string $token)
    {
        return $this->client->withHeaders(['Authorization' => $token])
            ->get('/users/current');
    }

    public function login(array $data)
    {
        return $this->client->post('/login', $data);
    }

    public function register(array $data)
    {
        return $this->client->post('/register', $data);
    }

    public function logout(string $token)
    {
        return $this->client->withHeaders(['Authorization' => $token])->post('/logout');
    }
}
