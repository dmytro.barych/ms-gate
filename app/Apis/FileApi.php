<?php

declare(strict_types=1);

namespace App\Apis;

use Illuminate\Support\Facades\Http;

class FileApi
{
    private $client;

    public function __construct()
    {
        $this->client = Http::withHeaders(
            [
                'Content-Type' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
                'Authorization' => request()->header('authorization') ?? null
            ]
        )->baseUrl(config('domains.files_domain'));
    }

    public function index(array $data = [])
    {
        return $this->client->get('/files', $data);
    }

    public function store(array $data)
    {
        return Http::asMultipart()
            ->baseUrl(config('domains.files_domain'))
            ->attach('file', fopen($data['file']->getRealPath(), 'rb'))
            ->post('/files', $data);
    }

    public function show(int $id)
    {
        return $this->client->get("/files/$id");
    }

    public function destroy(int $id)
    {
        return $this->client->delete("/files/$id");
    }
}
