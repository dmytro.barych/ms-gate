<?php

declare(strict_types=1);

namespace App\Apis;

use Illuminate\Support\Facades\Http;

class PostApi
{
    private $client;

    public function __construct()
    {
        $this->client = Http::withHeaders(
            [
                'Content-Type' => 'application/json',
                'X-Requested-With' => 'XMLHttpRequest',
                'Authorization' => request()->header('authorization') ?? null
            ]
        )->baseUrl(config('domains.posts_domain'));
    }

    public function index(array $data = [])
    {
        return $this->client->get('/posts', $data);
    }

    public function store(array $data)
    {
        return $this->client->post('/posts', array_merge(['user_id' => request()->authUser['id']], $data));
    }

    public function show(int $id)
    {
        return $this->client->get("/posts/$id");
    }

    public function update(int $id, array $data)
    {
        return $this->client->put("/posts/$id", $data);
    }

    public function destroy(int $id)
    {
        return $this->client->delete("/posts/$id");
    }

    public function toggleLike(int $id)
    {
        return $this->client->post("posts/$id/toggle-like", ['user_id' => request()->authUser['id']]);
    }

    public function categoryIndex(array $data = [])
    {
        return $this->client->get('/categories', $data);
    }

    public function categoryStore(array $data)
    {
        return $this->client->post('/categories', $data);
    }

    public function categoryShow(int $id)
    {
        return $this->client->get("/categories/$id");
    }

    public function categoryUpdate(int $id, array $data)
    {
        return $this->client->put("/categories/$id", $data);
    }

    public function categoryDestroy(int $id)
    {
        return $this->client->delete("/categories/$id");
    }

    public function tagIndex(array $data = [])
    {
        return $this->client->get('/tags', $data);
    }

    public function getPostsByCategory(int $id)
    {
        return $this->client->get("/categories/$id/posts");
    }

    public function getPostsByTag(int $id)
    {
        return $this->client->get("/tags/$id/posts");
    }
}
