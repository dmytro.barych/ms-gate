<?php

declare(strict_types=1);

namespace Tests\Feature;

use App\Facades\AuthApi;
use App\Http\Middleware\MsAuthMiddleware;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response as ResponseAlias;
use Tests\TestCase;

class MsAuthMiddlewareTest extends TestCase
{
    /**
     * @test
     */
    public function handleTest()
    {
        AuthApi::shouldReceive('user')
            ->once()
            ->andReturn([
                'data' => [
                    'id' => 1,
                    'email' => 'test@test',
                    'name' => 'Test'
                ]
            ]);

        $request = new Request;

        $request->headers->set('Authorization', 'test');

        $middleware = new MsAuthMiddleware;

        $response = $middleware->handle($request, function () {
        });

        //Null it's means middleware passed as have to but no next page for redirect
        $this->assertEquals($response, null);
    }

    /**
     * @test
     */
    public function handleWithWrongHeaderTest()
    {
        $request = new Request;

        $request->headers->set('Authorization', 'test');

        $middleware = new MsAuthMiddleware;

        $response = $middleware->handle($request, function () {
        });

        //Null it's means middleware passed as have to but no next page for redirect
        $this->assertEquals($response->status(), ResponseAlias::HTTP_UNAUTHORIZED);
    }

    /**
     * @test
     */
    public function handleWithoutHeaderTest()
    {
        $request = new Request;

        $middleware = new MsAuthMiddleware;

        $response = $middleware->handle($request, function () {
        });

        //Null it's means middleware passed as have to but no next page for redirect
        $this->assertEquals($response->status(), ResponseAlias::HTTP_UNAUTHORIZED);
    }
}
